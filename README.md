# Oglo Hyprland Rice Theme Translation Rust Implementation

An optional dependency for OgloTheNerd's Hyprland rice that increases the speed of changing the rice theme. (Because it replaces the Bash implementation with a Rust implementation.)
