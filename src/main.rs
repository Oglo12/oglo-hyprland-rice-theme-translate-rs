mod cli;

use clap::Parser;
use fspp::*;

#[derive(PartialEq, Eq)]
enum ExitCode {
    Success,
    Fail,
}

macro_rules! could_fail {
    (
        $function: expr
    ) => {
        {
            match $function {
                Ok(o) => o,
                Err(_) => return ExitCode::Fail,
            }
        }
    };
}

fn main() {
    if app() == ExitCode::Fail {
        std::process::exit(1);
    }
}

fn app() -> ExitCode {
    let args = cli::Cli::parse();

    let mut template: String = could_fail!(file::read(&Path::new(&args.template)));
    let theme_txt: String = could_fail!(file::read(&Path::new(&args.theme_txt)));

    let mut s_left = "";
    let mut s_right = "";

    let mut ignore_char = "";

    let lang = args.language;

    if lang == "scss" {
        s_left = "$";
    }
    
    else if lang == "css" {
        s_left = "@";
    }
    
    else if lang == "yml" {
        s_left = "${";
        s_right = "}";
    }
    
    else if lang == "toml" {
        s_left = "${";
        s_right = "}";
    }
    
    else if lang == "rasi" {
        s_left = "${";
        s_right = "}";
    }
    
    else if lang == "generic" {
        s_left = "${";
        s_right = "}";
    }
    
    else if lang == "hyprland-conf" {
        s_left = "%{";
        s_right = "}";

        ignore_char = "#";
    }

    for i in theme_txt.trim().split("\n") {
        let segments: Vec<&str> = i.trim().split(" -> ").collect();

        if segments.len() < 2 {
            return ExitCode::Fail;
        }

        let (key, mut value) = (segments[0].replace("$", ""), segments[1].replace(";", ""));

        if ignore_char.trim() != "" {
            value = value.replace(ignore_char, "");
        }

        template = template.replace(&format!("{}--{}--{}", s_left, key, s_right), &value);
    }

    could_fail!(file::write(&template, &Path::new(&args.generated)));

    return ExitCode::Success;
}
