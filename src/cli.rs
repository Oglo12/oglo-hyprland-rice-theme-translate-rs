use clap::Parser;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = true)]
pub struct Cli {
    #[clap(long)]
    pub template: String,
    #[clap(long)]
    pub generated: String,
    #[clap(long)]
    pub language: String,
    #[clap(long)]
    pub theme_txt: String,
}
